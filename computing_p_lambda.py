# Date: October 5, 2017
# Dinakar Muthiah
# This is an attempt to implement vector partitions
"""
Sample usage:

sage: RS = RootSystem(["A",3])
sage: Q = RS.root_lattice()
sage: a = Q.simple_root
sage: p_lambda(10*a(1)+10*a(2)+15*a(3))
t^35 - t^34 - t^33 + t^31 + t^30 - t^29 + t^23 - t^22 - t^21 + t^20
"""

from itertools import count
from sage.all import QQ, FunctionField, add

def is_componentwise_positive(vector):
    for component in vector:
        if component < 0:
            return False
    return True

def vector_partitions(vec,list_of_valid_vectors):
    """
    Return a list of vector partitions of ``vec`` 
    into vectors in ``list_of_valid_vectors``. 
    EXAMPLES::
        sage: vector_partitions(vector([1,1]),[vector([1,0]),vector([0,1]),vector([1,1])])

        [[((1, 1), 1), ((0, 1), 0), ((1, 0), 0)],
         [((1, 1), 0), ((0, 1), 1), ((1, 0), 1)]]
    """
    output = []
    if len(list_of_valid_vectors) == 0:
        if vec.is_zero():
            return [[]]
        else:
            return []
    else:
        first_vec = list_of_valid_vectors[0]
        for n in count():
            if is_componentwise_positive(vec - n*first_vec):
                for d in vector_partitions(
                        vec - n*first_vec,
                        list_of_valid_vectors[1:]):
                    output.append(d+[(first_vec,n)])
            else:
                break
    return output


def kostant_partitions(root_lattice_elem):
    """
    EXAMPLES::
        sage: RS = RootSystem(["A",2])
        sage: Q = RS.root_lattice()
        sage: a = Q.simple_root
        sage: kostant_partitions(a(1)+a(2))

        [[(alpha[1] + alpha[2], 1), (alpha[2], 0), (alpha[1], 0)],
         [(alpha[1] + alpha[2], 0), (alpha[2], 1), (alpha[1], 1)]]
    """
    Q = root_lattice_elem.parent()
    pos_roots_as_vectors = [b.to_vector() for b in Q.positive_roots()]
    kostant_partitions_in_vector_form = vector_partitions(
        root_lattice_elem.to_vector(),pos_roots_as_vectors)
    output = []
    for vect_partition in kostant_partitions_in_vector_form:
        output.append([(Q.from_vector(vec),coeff) for vec,coeff in vect_partition])
    return output

def support_of_kostant_partition(kostant_partition):
    """
    Return list of roots in the support of a kostant partition.
    """
    return [vec for vec,coeff in kostant_partition if coeff > 0]

def sum_of_coeffs_of_kostant_partition(kostant_partition):
    return add([coeff for _,coeff in kostant_partition])


def poincare_polynomial(subset_of_weyl_group,t):
    R = t.parent()
    terms = [t**l for l in (w.length() for w in subset_of_weyl_group)]
    return add(terms,R.zero())


def w_supp(weyl_group,support):
    output = []
    for w in weyl_group:
        def inverts_all():
            for b in support:
                if w.action(b).is_positive_root():
                    return False
            return True
        if inverts_all():
            output.append(w)
    return output
    


def contribution_from_given_kostant_partition(kostant_partition,weyl_group,t):
    supp = support_of_kostant_partition(kostant_partition)
    supp_in_weyl_group = w_supp(weyl_group,support_of_kostant_partition(kostant_partition))
    term = ((t**sum_of_coeffs_of_kostant_partition(kostant_partition))*
                    ((1-t**(-2))**len(supp))*
                    poincare_polynomial(supp_in_weyl_group,t))
    return term


def p_lambda(root_lattice_elem):
    """
    EXAMPLES::
        sage: RS = RootSystem(["A",3])
        sage: Q = RS.root_lattice()
        sage: a = Q.simple_root
        sage: p_lambda(10*a(1)+10*a(2)+15*a(3))
        t^35 - t^34 - t^33 + t^31 + t^30 - t^29 + t^23 - t^22 - t^21 + t^20
    """
    F = FunctionField(QQ,'t')
    t = F('t')
    Q = root_lattice_elem.parent()
    W = Q.weyl_group()
    P = poincare_polynomial(W,t)
    running_sum = F.zero()
    for kostant_partition in kostant_partitions(root_lattice_elem):
        term = contribution_from_given_kostant_partition(kostant_partition,W,t)
        running_sum += term
    return running_sum/P


    
