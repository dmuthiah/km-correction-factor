# Date: December 26, 2017
# Dinakar Muthiah
# This file is an attempt to compute hyperbolic correction
# factors in general


def vectors_below_a_certain_height(length,max_height):
    if length == 0:
        return [()]
    else:
        output = []
        for a in xrange(max_height+1):
            for vec in vectors_below_a_certain_height(length-1,max_height-a):
                output.append((a,)+vec)
    return output




class HyperbolicRootSystem(object):

    def __init__(self,cartan_matrix,symmetrizing_matrix):
        self.cartan_matrix = cartan_matrix
        self.rank = cartan_matrix.nrows()
        self.symmetrizing_matrix = symmetrizing_matrix
        self.symmetric_matrix = self.symmetrizing_matrix.inverse()*self.cartan_matrix


    def inner_prod(self,vec1,vec2):
        return (matrix(vec1)*self.symmetric_matrix*matrix(vec2).transpose())[(0,0)]

    def length_squared(self,vec):
        return self.inner_prod(vec,vec)

    def imaginary_roots_below_a_certain_height(self,max_height):
        output = []
        for vec in vectors_below_a_certain_height(self.rank,max_height):
            if self.inner_prod(vec,vec) <= 0:
                output.append(vec)
        return output

    def real_roots_below_a_certain_height(self,max_height):
        output = []
        for vec in vectors_below_a_certain_height(self.rank,max_height):
            length_squared = self.inner_prod(vec,vec)
            if length_squared > 0:
                include = True
                for i in xrange(self.rank):
                    if vec[i]*self.symmetric_matrix[(i,i)]/length_squared not in ZZ:
                        include = False
                        break
                if include:
                    output.append(vec)
        return output
            
    def is_antidominant(self,vec):
        pairing_with_simple_coroots = matrix(vec)*self.symmetric_matrix
        return all([c <= 0 for c in pairing_with_simple_coroots.list()])
        

    def simple_root(self,i):
        zeros = self.rank*[0]
        zeros[i-1] = 1
        return tuple(zeros)
        


def Delta(root_sys,bound,include_t=False): 
    R = PolynomialRing(QQ,'t')
    t = R('t')
    rank = root_sys.rank
    xvarnames = ["x%d" % i for i in xrange(1,rank+1)]
    S = PowerSeriesRing(R,xvarnames,default_prec=bound)
    output = S.one() + S.bigoh(bound)
    real_roots = root_sys.real_roots_below_a_certain_height(bound)
    for vec in real_roots:
        product = S.one()
        if include_t:
            product *= t
        x = S.gens()
        for i,veci in enumerate(vec):
            product *= x[i]**veci
        output *= 1 - product
    return output

class CorrectionFactorCalculator(object):

    def __init__(self,root_sys,ht_bound):
        self.root_sys = root_sys
        self.ht_bound = ht_bound

    def lowest_imaginary_factor(self,input_series):
        """
        """
        exponents = input_series.exponents() #Returned in increasing total degree order
        for exponent in exponents:
            if (self.root_sys.length_squared(tuple(exponent))<=0 and
                len(exponent.nonzero_values())!=0):
                if sum(exponent) <= self.ht_bound:
                    return exponent,input_series.dict()[exponent]
        return None

    def corrected_series(self,input_series,low_imag_factor):
        imag_exponent,imag_coeff = low_imag_factor
        S = parent(input_series)
        R = parent(imag_coeff)
        t = S('t')
        factor = S.one()
        for i,xi in enumerate(S.gens()):
            factor *= xi**imag_exponent[i]
        new_series = input_series
        for key,value in imag_coeff.dict().iteritems():
            new_factor = (S.one()+S.bigoh(self.ht_bound) -
                          (t**key)*factor)**value 
            new_series = new_series*new_factor
        return new_series
            

    def compute_correction_factor(self,input_series,running_factor_dict):
        S = parent(input_series)
        low_imag_factor = self.lowest_imaginary_factor(input_series)
        if low_imag_factor is None:
            return (S.one(),running_factor_dict)
        else:
            imag_exponent,imag_coeff = low_imag_factor 
            R = parent(imag_coeff)
            new_series = self.corrected_series(input_series,low_imag_factor)
            for key,value in imag_coeff.dict().iteritems():
                running_factor_dict[tuple(imag_exponent)] = imag_coeff 
            return self.compute_correction_factor(new_series,running_factor_dict)
