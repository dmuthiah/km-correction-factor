# Date: October 7, 2017
# Dinakar Muthiah
# This is an attempt to compute p_lambda in symmetric rank two hyperbolic cases

### This still needs to be tested against some examples


from sage.all import matrix,vector
from computing_p_lambda import vector_partitions


def flip_one_two(i):
    if i==1:
        return 2
    elif i==2:
        return 1
    else:
        raise ValueError

class RankTwoKacMoodyRootSystem(object):

    def __init__(self,upper_right_cartan_entry,lower_left_cartan_entry):
        # if upper_right_cartan_entry != lower_left_cartan_entry:
        #     raise NotImplementedError("Only symmetric Cartan matrices " 
        #                               "currently implemented")
        self.off_diagonal_entries = (upper_right_cartan_entry,
                                     lower_left_cartan_entry)

    def simple_reflection(self,i):
        a,b = self.off_diagonal_entries
        if i==1:
            return matrix([[-1,a],[0,1]])
        if i==2:
            return matrix([[1,0],[b,-1]])

    def simple_root(self,i):
        if i==1:
            return vector([1,0])
        if i==2:
            return vector([0,1])


    def real_root_series(self,i,num_roots):
        if num_roots==0:
            return []
        else:
            return ([self.simple_root(i)] +
                    [self.simple_reflection(i)*root for
                     root in self.real_root_series(flip_one_two(i),num_roots-1)])



def series_i_partitions(rank_two_system,i,root_lattice_vector,num_roots=None):
    if num_roots is None:
        num_roots = max(*tuple(root_lattice_vector))
    real_root_series = rank_two_system.real_root_series(i,num_roots)
    return vector_partitions(root_lattice_vector,real_root_series)

def p_lambda(rank_two_system,root_lattice_vector):
    F = FunctionField(QQ,'t')
    t = F('t')
    running_sum = F.zero()
    for i in [1,2]:
        for vector_partition in series_i_partitions(rank_two_system,i,root_lattice_vector):
            N = max(n for n,(_,coeff) in enumerate(vector_partition) if coeff > 0)+1
            supp_size = len([vec for vec,coeff in vector_partition if coeff > 0])
            sum_of_coeffs = add([coeff for _,coeff in vector_partition if coeff > 0])
            running_sum += ((t**(sum_of_coeffs+N-2*supp_size)*
                             (t**2-1)**supp_size)/(1+t))
    return running_sum
