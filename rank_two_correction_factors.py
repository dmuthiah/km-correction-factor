# Date: October 17, 2016
# Dinakar Muthiah
# This file is an attempt to compute hyperbolic correction 
# factors using the very nice algorithm explained to me
# by Ian Whitehead

"""
Sample usage:

In the terminal, go to the folder containing this file
(rank_two_correction_factors.py).
Run sage by typing sage and hitting enter. Then run:

sage: load("rank_two_correction_factors.py")
sage: C = RankTwoHyperbolicRootSystem(3,3); calc = CorrectionFactorCalculator(C,8)
# Here 8 is the height bound. You can make it larger for more data
sage: computed_correction_factor = calc.correction_for_Delta_over_Delta_t()
sage: formatter = CorrectionFactorFormatter(computed_correction_factor)
sage: formatter.correction_factor_as_pairs()
# This will output the correction factor as a set of pairs
sage: formatter.correction_factor_expanded(height_bound=8)
# This will expand the correction factor as series (up to precision 8 in this case)
"""

from sage.all import PolynomialRing, PowerSeriesRing, QQ, parent


class RankTwoHyperbolicRootSystem(object):

    def __init__(self,upper_right_cartan_entry,lower_left_cartan_entry):
        if upper_right_cartan_entry != lower_left_cartan_entry:
            raise NotImplementedError("Only symmetric Cartan matrices " 
                                      "currently implemented")
        self.off_diagonal_entries = (upper_right_cartan_entry,
                                     lower_left_cartan_entry)

    def imaginary_roots_below_a_certain_bound(self,bound):
        output = []
        for a1 in xrange(bound+1):
            for a2 in xrange(bound+1-a1):
                if self.length_square((a1,a2)) <= 0:
                    output.append((a1,a2))
        return output


    def real_roots_below_a_certain_bound(self,bound):
        output = []
        for a1 in xrange(bound+1):
            for a2 in xrange(bound+1-a1):
                if self.length_square((a1,a2)) == 2:
                    output.append((a1,a2))
        return output

    def length_square(self,root_lattice_elem):
        a1,a2 = root_lattice_elem
        a = self.off_diagonal_entries[0]
        return 2*a1**2 - 2*a*a1*a2 + 2*a2**2


def Delta(rank_two_system,bound,include_t=False):
    R = PolynomialRing(QQ,'t')
    S = PowerSeriesRing(R,'x1,x2',default_prec=bound)
    t,x1,x2 = S('t'),S('x1'),S('x2')
    output = S.one() + S.bigoh(bound)
    real_roots = rank_two_system.real_roots_below_a_certain_bound(bound)
    for a1,a2 in real_roots:
        if include_t:
            output *= 1 - t*(x1**a1)*(x2**a2) 
        else:
            output *= 1 - (x1**a1)*(x2**a2) 
    return output


class CorrectionFactorCalculator(object):

    def __init__(self,rank_two_system,height_bound):
        self.rank_two_system = rank_two_system
        self.height_bound = height_bound

    def correction_for_Delta(self):
        input_series = Delta(self.rank_two_system,self.height_bound,
                             include_t=False)
        return self.compute_correction_factor(input_series,{})

    def correction_for_Delta_over_Delta_t(self):
        input_numerator = Delta(self.rank_two_system,self.height_bound,
                                include_t=False)
        input_denominator = Delta(self.rank_two_system,self.height_bound,
                                  include_t=True)
        input_series = input_numerator/input_denominator
        return self.compute_correction_factor(input_series,{})

    def lowest_imaginary_factor(self,input_series):
        """
        """
        exponents = input_series.exponents() #Returned in increasing total degree order
        for exponent in exponents:
            if (self.rank_two_system.length_square(exponent)<=0 and
                len(exponent.nonzero_values())!=0):
                if sum(exponent) <= self.height_bound:
                    return exponent,input_series.dict()[exponent]
        return None

    def compute_correction_factor(self,input_series,running_factor_dict):
        S = parent(input_series)
        low_imag_factor = self.lowest_imaginary_factor(input_series)
        if low_imag_factor is None:
            return (S.one(),running_factor_dict)
        else:
            imag_exponent,imag_coeff = low_imag_factor 
            R = parent(imag_coeff)
            new_series = self.corrected_series(input_series,low_imag_factor)
            for key,value in imag_coeff.dict().iteritems():
                a1,a2 = imag_exponent
                running_factor_dict[(key,a1,a2)] = value
            return self.compute_correction_factor(new_series,running_factor_dict)


    def corrected_series(self,input_series,low_imag_factor):
        imag_exponent,imag_coeff = low_imag_factor
        a1,a2 = imag_exponent
        S = parent(input_series)
        R = parent(imag_coeff)
        t = S('t')
        x1,x2 = S('x1'),S('x2')
        new_series = input_series
        for key,value in imag_coeff.dict().iteritems():
            new_factor = (S.one()+S.bigoh(self.height_bound) -
                          (t**key)*(x1**a1)*(x2**a2))**value 
            new_series = new_series*new_factor
        return new_series




class CorrectionFactorFormatter(object):

    def __init__(self,computed_correction_factor):
        self.remainder = computed_correction_factor[0]
        self.base_ring = parent(self.remainder)
        self.correction_factor_dict = computed_correction_factor[1]


    def correction_factor_as_dict(self):
        return self.correction_factor_dict

    def individual_factor(self,exponent):
        t_exp,x1_exp,x2_exp = exponent
        S = self.base_ring
        t,x1,x2 = S('t'),S('x1'),S('x2')
        return (1 - (t**t_exp)*(x1**x1_exp)*(x2**x2_exp))
        

    def correction_factor_as_pairs(self,sort_cmp=None):
        items = self.correction_factor_dict.items()
        sorted_items = sorted(items,cmp=sort_cmp)
        output = []
        for key,value in sorted_items:
            output.append((self.individual_factor(key),value))
        return output

    def correction_factor_expanded(self,height_bound):
        S = self.base_ring
        output = S.one()
        for factor,exponent in self.correction_factor_as_pairs():
            output *= (S.bigoh(height_bound) + factor)**exponent
        return output

        
